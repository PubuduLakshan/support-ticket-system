
<html>
@extends('layouts.header')
<body>

<div class="container">
  <h2>Online Support System</h2>
 @foreach($errors->all() as $error)
<div class="alert alert-danger">{{$error}}</div>
 @endforeach
 
 <form method="post" action="/saveTicket">
 {{csrf_field()}}

 <div class="form-group">
    <label for="name">Customer Name:</label>
    <input type="name" class="form-control" id="name" name="name">
  </div>

  <div class="form-group">
    <label for="problem">Problem Description:</label>
    <textarea type="problem" class="form-control" rows="5" id="problem" name="problem"></textarea>
  </div>

  <div class="form-group">
    <label for="email">Email address:</label>
    <input type="email" class="form-control" id="email" name="email">
  </div>

  <div class="form-group">
    <label for="phone">Phone Number:</label>
    <input type="phone" class="form-control" id="phone" name="phone">
  </div>

  <button type="submit" class="btn btn-default">Submit</button>
</form> 
</div>
</body>
</html>