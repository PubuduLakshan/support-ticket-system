@extends('layouts.app')

@extends('layouts.header')

@section('content')



<div class="container">
  <h2>Online Support System</h2>
          
  <table class="table" id="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Customer Name</th>
        <th>Email</th>
        <th>Phone Number</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($ticket as $tickets)
      <tr>
        <td>{{$tickets->id}}</td>
        <td>{{$tickets->Customer_Name}}</td>
        <td>{{$tickets->Email}}</td>
        <td>{{$tickets->Phone_No}}</td>
        @if($tickets->Is_Completed)
        <td><span class="label label-success">Completed</span></td>
        @else
        <td><span class="label label-danger">Not Completed</span></td>
        @endif
        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" class="btn"><i class="fa fa-edit"></i></button></td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>


  <!-- Edit Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Address the Problem</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="/saveTicket">
 {{csrf_field()}}

 <div class="form-group">
    <label for="name">Customer Name:</label>
    <input type="name" class="form-control" id="name" name="name">
  </div>

  <div class="form-group">
    <label for="problem">Problem Description:</label>
    <textarea type="problem" class="form-control" rows="5" id="problem" name="problem"></textarea>
  </div>

  <div class="form-group">
    <label for="respond">Respond:</label>
    <textarea type="respond" class="form-control" rows="5" id="respond" name="respond"></textarea>
  </div>

  <button type="submit" class="btn btn-default">Submit</button>
</form> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



@endsection


