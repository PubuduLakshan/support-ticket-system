<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/saveTicket','TicketController@store');

Route::get('/ticket','Ticket_details_Controller@Ticket_details');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/support','showDetails@show');
