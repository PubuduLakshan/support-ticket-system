<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ticket;
use Mail;

class TicketController extends Controller
{
    public function store(Request $request){
        
        // dd($request->all());

            $this->validate($request,[
            'name'=>'required|max:50|min:5',
            'problem'=>'required|max:50|min:5',
            'email'=>'required|email',
            'phone'=>'required|regex:/^(0)[0-9]{9}/|numeric',

        ]);

         $ticket=new ticket;
         $ticket->Customer_Name = $request->name;
         $ticket->Problem_description = $request->problem;
         $ticket->Email = $request->email;
         $ticket->Phone_No = $request->phone;
         $ticket->Reference_No = uniqid();
         $ticket->save();

         $a=$ticket->Reference_No;
         $data = array(
            "email" => $request->email,
            "description" => $a
          );

         Mail::send('emails.contacts',$data,function($message) use ($data){
             $message->from('pubudu.lakshan7@gmail.com');
             $message->to($data['email']);
             $message->subject($data['description']);

         });

        // $data=$ticket::all();
        // dd($data);
        // return view('NewTickets');

       return redirect()->back();
        
    

    }
}
