<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ticket;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('home');
        $ticket=new ticket;

        $data=$ticket::all();
        // dd($data);

        return view('home')->with('ticket',$data);
    }
}
